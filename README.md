

# How to build
```
g++ -std=c++11 intuwarePVision.cpp -o intuwarePVision
```

# Description Arguments
```
#########################################################################
A intuwarePVision How to usage
Usage:
  intuwarePVision [OPTION...]

  -t, --tagdir arg   tagged directory path
  -p, --pid          Process id file path
  -l, --logfile arg  log file path
  -n, --ngdir arg    NG directory path
  -o, --okdir arg    OK directory path
  -h, --help         Print usage
#########################################################################
```