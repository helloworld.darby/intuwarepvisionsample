#include "cxxopts.hpp"

/*
("p,pid", "Process id file path", cxxopts::value<bool>()->default_value("false"))
("l,logfile", "log file path", cxxopts::value<int>()->default_value("10"))
*/
void show_usage(cxxopts::Options options) {
    std::cout << "#########################################################################" << std::endl;
    std::cout << options.help() << std::endl;
    std::cout << "#########################################################################" << std::endl;   
}
int main(int argc, char** argv)
{
    cxxopts::Options options("intuwarePVision", "A intuwarePVision How to usage");
    options.add_options()
        ("t,tagdir", "tagged directory path", cxxopts::value<std::string>())
        ("p,pid", "Process id file path", cxxopts::value<std::string>())
        ("l,logfile", "log file path", cxxopts::value<std::string>())
        ("n,ngdir", "NG directory path", cxxopts::value<std::string>())
        ("o,okdir", "OK directory path", cxxopts::value<std::string>())
        ("h,help", "Print usage")
    ;
    try
    {
        auto result = options.parse(argc, argv); 

        std::cout << "help:" << result.count("help") << std::endl;

        if (result.count("help"))
        {
            show_usage(options);
            exit(0);
        }
        // bool debug = result["debug"].as<bool>();
        std::string tagdir;
        if (result.count("tagdir")) {
            tagdir = result["tagdir"].as<std::string>();
            std::cout << "tagdir:" << tagdir << std::endl;
        }
    }
    catch(const std::exception& e)
    {
        //std::cerr << e.what() << '\n';
        show_usage(options);
        exit(0);
    } 
    return 0;
}